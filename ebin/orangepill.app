{application,orangepill,
             [{description,"A peer node implementation for the Orange Pill Storage System"},
              {registered,[]},
              {included_applications,[]},
              {applications,[stdlib,kernel]},
              {vsn,"0.1.0"},
              {modules,[opss_client,opss_client_man,opss_client_sup,
                        opss_clients,opss_sup,orangepill]},
              {mod,{orangepill,[]}}]}.
