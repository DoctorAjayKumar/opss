## Introduction: the situation we're in

Hi, everyone

I posted an older version of this in a comment somewhere, but I'm reposting it
as a top-level thread for visibility.

Larry posted on his blog an article called [*What
Decentralization
Requires*](https://larrysanger.org/2021/01/what-decentralization-requires/),
and asked people to comment with agree or disagree.

At the time, I couldn't produce a good linguistic approximation of my thoughts
on the matter.

By analogy, I came up with the following: that article is proposing a set of
traffic laws for flying cars. It's not possible for me to meaningfully agree or
disagree with it, because I don't know what constraints flying car technology
imposes, because flying car technology doesn't exist yet.

Postulating about high-level rules for what a decentralized internet might look
like is not a useful exercise until we know what constraints the low-level
technology imposes. That insight cannot come until the technology exists and is
widely used.

**The basic technology required to create a truly decentralized internet does
not currently exist.**

In particular, creating decentralized replacements for social networks and
video hosting sites does **NOT** amount to simply writing browser extensions,
WordPress plugins, or web apps.

The ideas that I'm seeing proposed on this forum are akin to suggesting that we
build flying cars by attaching eagles to horse-drawn carriages.

As an analogy: before you write a web app, you need HTTP to exist. Before HTTP,
you need TCP. The decentralized web is at the stage where it doesn't have TCP
yet.

Let me now pivot to an anecdote, and I will circle back and explain its
relevance.

## Anecdote: Parler

Parler has been canceled. It's all over the news, but if you aren't familiar
with the story, here are two articles:

1. <https://archive.vn/33i7a>
2. <https://archive.vn/4jaBd>

Additionally, there are unconfirmed rumors that

1. [Parler suffered a database breach that has led to leakage of private user
   data such as cell phone numbers and scans of photo
   IDs](https://archive.vn/AFSa8)

2. [Parler failed to scrub metadata from files uploaded to the website (such as
   geotagging in photos), resulting in leakage of identifying information such
   as GPS coordinates where photos were taken.](https://archive.vn/7i8kO)

I haven't seen any concrete evidence of the latter two claims, just reports.

According to those articles, Parler is built on WordPress. WordPress is
notoriously insecure. Supposing that it is true that Parler is built on
WordPress, leakage of user data shouldn't surprise anyone. For instance, [the
Panama Papers leak was in part made possible by poorly-written WordPress
plugins](https://archive.vn/APr5Y).  Moreover, Parler chose to build their
house on enemy turf (i.e. AWS), with predictable results.

Bottom line: the reason Parler is in the situation they are in is because they
made every bad design decision one could possibly make.

## Circling back

Parler teaches us an important point: we cannot skip over getting the low-level
technical details right. Duct taping existing garbage technology together is
not an adequate vision for a decentralized internet.

There is no off-the-shelf system with all of the necessary properties. There is
no getting around the fact that getting the infrastructure *right* and properly
implemented is a necessary precondition to application development. Look no
further than the Parler debacle.

Like I said, the ideas that I'm seeing proposed on this forum are akin to
suggesting that we build flying cars by attaching eagles to horse-drawn
carriages.


## A solution

For the last few months, Craig Everett (@zxq9) and I have been discussing
creating some technological replacement to solve the cancelation problem. The
time has clearly come for us to do that.

Our project is called The Orange Pill. The first component is the low-level
data management software, which is called the Orange Pill Storage System, or
OPSS.

We have a [GitLab repository](https://gitlab.com/DoctorAjayKumar/opss) right
now, which also includes a [draft of a
manifesto](https://gitlab.com/DoctorAjayKumar/opss/-/blob/master/opss_manifesto).

That manifesto will eventually answer obvious questions such as

- "Why are you creating a new thing?"
- "Why don't you just use `$ExistingThing`?"
- "Why does OPSS solve `$Problem` and `$ExistingThing` doesn't?"
- "What exactly does OPSS do, and what does it not do?"
- "How does OPSS work?"
- "How can I help?"
- "How can I donate?"

Craig is an experienced distributed systems engineer, and I am a mathematician.

Craig is working right now on a proof of concept, as well as what amounts to a
whitepaper.  I am working on getting a funding infrastructure set up, so that
Craig and I can afford to work on this. We anticipate 3-12 months of full time
work is required to get to version 1.

If someone has some experience setting up funding infrastructure for
open-source projects, I could really use some pointers. I am going off of what
I find on Google, and God only knows if any of it is correct.

You can contact me on Twitter (@DoctorAjayKumar), or via email (same username
at protonmail)

Craig is @zxq9 here and most places, @zxq9_notits on Twitter. His website is
<http://zxq9.com/>
